import AutoImport from 'unplugin-auto-import/vite'
import {ElementPlusResolver} from 'unplugin-vue-components/resolvers'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import {defineConfig, loadEnv} from 'vite'

import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import VueSetupExtend from 'vite-plugin-vue-setup-extend'
import * as path from "path";

export default defineConfig(({ mode, command }) => {
    const env = loadEnv(mode, process.cwd())
    const {VITE_APP_ENV} = env;
    let esBuild: any = [];
    if (mode === 'production') {
        esBuild = ['console', 'debugger'];
    }

    return {
        base: VITE_APP_ENV === 'production' ? '/' : '/',
        plugins: [
            vue(),
            vueJsx(),
            VueSetupExtend(),
            AutoImport({
                resolvers: [
                    ElementPlusResolver()
                ],
            }),
            createSvgIconsPlugin({
                iconDirs: [path.resolve(process.cwd(), 'src/assets/icons/svg')],
                symbolId: 'icon-[dir]-[name]',
                svgoOptions: command === 'build'
            })
        ],

        resolve: {
            alias: {
                "@": path.join(__dirname, "./src"),
                //'@': fileURLToPath(new URL('./src', import.meta.url))
            }
        },
        server: {
            port: 6010,
            host: true,
            open: true,
        },
        esbuild: {
            drop: esBuild,
        },
    }
})
