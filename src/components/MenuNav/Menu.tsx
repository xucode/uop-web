import {computed, defineComponent, type PropType, ref, unref} from 'vue'
import Icon from '@/components/common/icon/index.vue'
import {ElMenu, ElMenuItem, ElSubMenu} from 'element-plus'

import 'element-plus/theme-chalk/el-menu.css'
import 'element-plus/theme-chalk/el-menu-item.css'
import 'element-plus/theme-chalk/el-sub-menu.css'
import './menu.css'
import {MENU_TYPE, VAR_TAG} from "@/stores/conf";

export default defineComponent({
    props: {
        isCollapse: {
            type: Boolean,
        },
        activeIndex: {
            type: String,
        },
        menus: {
            type: Array as PropType<MenuType[]>,
            required: true
        }
    },
    setup(props, context) {
        // let currRoute: Ref<RouteLocationNormalizedLoaded>;
        const isCollapseRef = ref(props.isCollapse)
        const menus: MenuType[] = props.menus || [];
        const menuList = computed(() => menus);

        function createIcon(icon: string) {
            return <Icon icon={icon} size={18}/>
        }

        const updateCollapse = (val: boolean) => {
            isCollapseRef.value = val;
        }

        const onSelectMenu = (path: string) => {

        }

        context.expose({
            updateCollapse
        })

        // 创建条目
        function createMenuItem(menuData: any[] = []) {
            // VNode节点数组
            return menuData.map((item: MenuType) => {
                if (!item.hidden) {
                    const menuIndex = item.routePath.replace(VAR_TAG, '');

                    // 如果不存在下级菜单 或 隐藏下级菜单则渲染item
                    if (MENU_TYPE.MENU === item.menuType || !item.children || !item.children.length) {
                        return (
                            <ElMenuItem index={menuIndex}>
                                {item.icon && createIcon(item.icon)}
                                <span>{item.name}</span>
                            </ElMenuItem>
                        );
                    } else {
                        // 存在下级菜单时，渲染sub-item
                        // 插槽,以对象形式插入到submenu标签上，下面插入了title插槽
                        const slots = {
                            title: () => {
                                return (
                                    <div>
                                        {item.icon && createIcon(item.icon)}
                                        <span class="sub-menu-text">{item.name}</span>
                                    </div>
                                )
                            }
                        }
                        return (
                            <ElSubMenu index={menuIndex} v-slots={slots}>
                                {createMenuItem(item.children)}
                            </ElSubMenu>
                        )
                    }
                }
            });
        }

        return () => <ElMenu
            defaultActive={props.activeIndex}
            onSelect={onSelectMenu}
            class="menuClass"
            collapse={unref(isCollapseRef) as unknown as boolean}>
            {createMenuItem(unref(menuList))}
        </ElMenu>
    },
})
