type TableColumn = {
    id: string;
    name: string;
    sort: boolean;
    show: boolean;
    width?: number;
    operations?: [
        {
            name: string;
            callback: any;
        }
    ]
}

