declare interface MenuType {
    id: string;
    menuType: number;
    name: string;
    routePath: string;
    routeName: string;
    component: string;
    valid?: boolean | undefined;
    icon: string;
    mode: string;
    hidden?: boolean;
    keepAlive?: boolean;
    meta: {
        permits?: Array | [];
    };
    children?: MenuType[];
}

declare type UserType = {
    id: string,
    name: string,
    headIcon?: string,
    headUrl?: string,
}

declare interface SearchItem {
    type: string;
    subType?: string;
    name: string;
    label?: string;
    labels?: string[];
    value?: any;
    defValue?: any;
    size?: string;
    placeholder?: string;
    placeholders?: string[];
    clearable?: boolean;
    format?: string;
    valFormat?: string;
}

declare interface SearchAction {
    type?: string;
    name: string;
    label: string;
    icon?: string;
    size?: string;
    action?: any;
}

declare interface Pagination {
    current: number,
    pageSize: number,
    total: number,
    sizes?: any,
}

declare interface SearchConfig {
    hasSelect?: boolean;
    showSearch?: boolean;
    toolMode?: string;
    title?: string;
    hidePageable?: boolean | undefined | true | false;
    defSearchAction?: true | undefined;
    search?: SearchItem[] | undefined;
    extraSearch?: SearchItem[] | undefined;
    searchAction?: SearchAction[] | undefined;
    tableAction?: SearchAction[] | undefined;
    setting: {
        showMoreSearch?: boolean | undefined;
        showMoreButton?: boolean | undefined;
        disableMoreButton?: boolean | undefined;
        searchBackground?: string | undefined;
        extraSearchBackground?: string | undefined;
        rootBackground?: string | undefined;
        showBorder?: boolean;
        tableBorder?: boolean;
        tableToolBackground?: string | undefined;
        tableToolRound?: string | undefined;
        pageMargin?: string | undefined;
        tableMargin?: string | undefined;
        tablePadding?: string | undefined;
        topSearchLayout?: string | undefined;
    };
    layout: {
        buttonSize?: string | undefined;
        actionButtonSize?: string | undefined;
        inputSize?: string | undefined;
    };
}
