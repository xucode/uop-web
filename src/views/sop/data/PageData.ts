export function buildSearchForms(tag: string, productInfo: any=undefined) {
    const searchForms = [];

    if (productInfo && productInfo.show) {
        searchForms.push({
            type: 'select',
            name: 'productCode',
            label: `产品类型`,
            size: 'default',
            placeholder: `请选择产品类型`,
            clearable: true,
            defValue: 'default',
            minWidth: '120px',
            onChanged: productInfo.onChanged,
            options: [
                {
                    label:'默认',
                    value: 'default',
                },
                {
                    label: '客户端',
                    value: 'cus',
                },
            ]
        });
    }
    searchForms.push({
            type: 'select',
            name: 'state',
            label: '状态',
            size: 'default',
            placeholder: '',
            clearable: true,
            defValue: -1,
            width: '130px',
            options: [
                {
                    label: '全部',
                    value: -1,
                },
                {
                    label: '启用',
                    value: 1,
                },
                {
                    label: '禁用',
                    value: 2,
                }
            ]
        });

    searchForms.push({
            type: 'input',
            name: 'name',
            label: `${tag}名称`,
            size: 'default',
            placeholder: `请输入${tag}名称`,
            clearable: true,
            defValue: '',
        });

    return searchForms;
}

export function groupSearchForms(productInfo: any=undefined) {
    return buildSearchForms('分组', productInfo);
}
