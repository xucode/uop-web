import {shallowRef} from "vue";
import type {RouteRecordRaw} from 'vue-router';
import {MENU_TYPE, VAR_TAG} from "@/stores/conf";

const modules = import.meta.glob("../views/pages/**/*.vue");

const findComponent = (name: string) => {
    const newName = `../views/pages${name}Page.vue`;
    for (const key in modules) {
        if (key.toLowerCase() === newName.toLowerCase()) {
            return modules[key];
        }
    }
    return undefined;
}

const normalizePath = (parent: string | undefined, path: string) => {
    const newPath: string = !parent || path.startsWith('/') ? path : `${parent}/${path}`;
    if (!newPath.startsWith('/')) {
        return '/' + newPath;
    } else {
        return newPath;
    }
}

const buildMenuMeta = (item: any) => {
    let varPath: string = '';
    const pos: number = item.routePath?.indexOf(VAR_TAG);
    if (pos != -1) {
        varPath = item.routePath.replace(VAR_TAG, '');
    }
    return {
        title: item.name,
        icon: item.icon,
        type: item.menuType,
        keepAlive: item.keepAlive,
        varPath: varPath,
    };
}

const handleTopMenu = (store: any, insertMenus: MenuType[], rootRoute: RouteRecordRaw, m: any): boolean => {
    /*
     * 这里顶部菜单做了二级菜单拆分，点击顶部菜单，导航到第一级路由页面
     * 第一级路由页面可能是单纯的简单页面，也可能是包含了二级路由的页面
     * 这里做这个处理，主要方便后台对整个系统菜单做统一配置，前端根据需要把第一级拆掉拆分到顶部显示了
     */
    let menuComp = m.component;
    const topMap = store.topMenuMap;

    if (!menuComp || !topMap) {
        return false;
    }

    if (menuComp.startsWith('/')) {
        menuComp = menuComp.substring(1);
    }

    const mapMenu = topMap[menuComp];

    if (mapMenu && mapMenu.routeName && mapMenu.routePath && mapMenu.component) {
        const newMenu = {
            ...m
        }

        delete m.children;

        newMenu.routeName = mapMenu.routeName;
        newMenu.routePath = mapMenu.routePath;
        newMenu.component = mapMenu.component;

        const newRoute = {
            path: newMenu.routePath,
            name: newMenu.routeName,
            meta: {},
            component: shallowRef(findComponent(newMenu.component)),
        };

        newRoute.meta = buildMenuMeta(newMenu);

        insertMenus.push(newMenu);

        if (!rootRoute.children) {
            rootRoute.children = [];
        }

        rootRoute.children.push(newRoute);

        generateChildRoutes(store, newRoute, newMenu.children, newMenu);

        return true;
    } else {
        return false;
    }
}

const handleMenuPermits = (hasKids: boolean | undefined, m: MenuType) => {
    if (hasKids && m.menuType === MENU_TYPE.MENU) {
        // 这里需要建立菜单项的按钮权限
        if (!m.meta) {
            m.meta = {};
        }

        m.meta.permits = [];
        m.children?.forEach(item => {
            m.meta.permits.push({
                ...item,
            });
        });
        // delete m.children;
    }
}

/**
 * 把从服务端获取的菜单信息转换为VUE的路由信息
 * @param store
 * @param rootRoute 父菜单信息
 * @param menuData 服务端返回的菜单列表
 * @param parent
 */
const generateChildRoutes = (store: any, rootRoute: RouteRecordRaw,
                             menuData: MenuType[] | undefined,
                             parent: MenuType | undefined) => {
    if (menuData) {
        /*
         * insertMenus保存对原始菜单进行转换后需要插入的菜单项
         * 因为这里把菜单拆分为顶部和左边两级不同的菜单，分别是独立的菜单
         */
        const insertMenus: MenuType[] = [];

        menuData.forEach(m => {
            if (m.menuType === MENU_TYPE.DIR || m.menuType === MENU_TYPE.MENU) {
                const oriComp: string = m.component;
                m.routePath = normalizePath(parent?.routePath, m.routePath);
                m.component = normalizePath(parent?.component, oriComp);

                const hasKids: undefined | boolean = m.children && m.children.length > 0;
                let comp = findComponent(m.component);

                if (!comp && m.menuType === MENU_TYPE.MENU) {
                    const pos: number = parent?.component.lastIndexOf('/') || -1;
                    console.log('--- 找不到组件, m.component, pos：', m.component, pos);
                    if (pos > 0) {
                        const pComp = parent?.component.substring(0, pos);
                        m.component = normalizePath(pComp, oriComp);
                        console.log('--- generateChildRouters, m.component 11：', pComp, m.component);
                        comp = findComponent(m.component);
                    }
                }

                const name = `../views/pages${m.component}Page.vue`;

                m.valid = hasKids;

                /*
                 *  处理菜单项的权限，把其规整到meta.permits里
                 */
                handleMenuPermits(hasKids, m);

                if (!rootRoute.children) {
                    rootRoute.children = [];
                }

                let newRoute: RouteRecordRaw;

                if (comp) {
                    newRoute = {
                        path: m.routePath,
                        name: m.routeName,
                        component: shallowRef(comp),
                    };
                    newRoute.meta = buildMenuMeta(m);
                    rootRoute.children.push(newRoute);
                } else {
                    console.log('--- generateChildRouters, 组件不存在：', name);
                    /*
                     * 这里的菜单属于目录类型的，没有直接对应的组件页面
                     */
                    newRoute = rootRoute;
                }

                /**
                 * 菜单类型为目录才会建立其子路由
                 */
                if (hasKids && !handleTopMenu(store, insertMenus, rootRoute, m)) {
                    generateChildRoutes(store, newRoute, m.children, m);
                }
            }
        });

        /*
         * 最后把拆分好的菜单插入到根菜单列表后面
         */
        insertMenus.forEach(item => menuData.push(item));
    }
}

export const generateMainRoute = (mainLay: any, store: any, menuData: MenuType[]) => {
    const routes: RouteRecordRaw[] = [];
    const rootRoute: RouteRecordRaw = {
        path: '/user/login',
        name: 'login',
        component: shallowRef(mainLay),
        redirect: '/user/login',
        meta: {
            title: "登录",
            icon: "Grid",
            mode:"vertical",
            keepAlive: true,
        },
        children: []
    };

    generateChildRoutes(store, rootRoute, menuData, undefined);
    routes.push(rootRoute);

    return routes;
}
