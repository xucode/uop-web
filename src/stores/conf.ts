export const VAR_TAG: string = '/:id*';
export const CACHE_INCLUDED_ROUTES: string = 'CACHE_INCLUDED_ROUTES';

export const MENU_TYPE = {
    'DIR': 0,
    'MENU': 1,
    'BUTTON': 2,
};

export const currentNames = {
    'MENU': 'MENU',
    'CLIENT': 'CLIENT',
    'ROLE': 'ROLE',
    'PERMIT': 'PERMIT',
    'URL': 'URL',
    'API': 'API',
    'TENANT': 'TENANT',
    'APPLICATION': 'APPLICATION',
    'GROUP': 'GROUP',
    'PLAT_GROUP': 'PLAT_GROUP',
    'USER_GROUP': 'USER_GROUP',
    'REALM_USER': 'REALM_USER',
    'PLAT_USER': 'PLAT_USER',
    'REALM': 'REALM',
    'PLATFORM': 'PLATFORM',
    'TERMINAL': 'TERMINAL',
    'PLAT_REALM_USER': 'PLAT_REALM_USER',

    'LOGIN_LOG': 'LOGIN_LOG',
    'CALL_LOG': 'CALL_LOG',
    'AUDIT_LOG': 'AUDIT_LOG',

    'PLAT_DICT': 'PLAT_DICT',
    'PLAT_TERMINAL': 'PLAT_TERMINAL',
    'PLAT_LABELS': 'PLAT_LABELS',
    'PLAT_MODELS': 'PLAT_MODELS',
};

export const TYPES = {
    'PERMIT': 'PERMIT',
    'MENU': 'MENU',
    'ROLE': 'ROLE',
    'URL': 'URL',
    'API': 'API',
    'CLIENT': 'CLIENT',
    'GROUP': 'GROUP',
    'USER': 'USER',
    'REALM_USER': 'REALM_USER',
    'USER_GROUP': 'USER_GROUP',
    'REALM': 'REALM',
    'PLATFORM': 'PLATFORM',
    'TERMINAL': 'TERMINAL',
};

export const SERVERS = {
    'ASSET': 'ASSET',
    'REALM': 'REALM',
    'PLATFORM': 'PLATFORM',
};
