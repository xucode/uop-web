import SvgIcon from '@/components/common/svgicon/index.vue'

//全局对象
const allGlobalComponents = {SvgIcon}

//对外暴露插件对象
export default {
    install(app: any) {
        //注册项目的全部组件
        Object.keys(allGlobalComponents).forEach((key) => {
        //注册为全局组件
            app.component(key, allGlobalComponents[key])
        })
    },
}

