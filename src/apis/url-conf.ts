/**
 * 登录平台的接口地址
 */
const authUserLoginUrl = (prefix: string) => `${prefix}/uni-api/v1/auth/login`;

/**
 * 获取登录后用户资源的地址
 */
const authUserAssetLoadUrl = (prefix: string) => `${prefix}/uni-api/v1/auth/query/user-assets`;

const loginByCodeUrl = (prefix: string, code: string) => `${prefix}/uni-api/v1/sso/ticket?code=${code}`;
const authUserTokenUrl = (prefix: string) => `${prefix}/uni-api/v1/auth/token`;
const authUserAuthorizeUrl = (prefix: string) => `${prefix}/uni-api/v1/auth/authorize`;
const authUserLogoutUrl = (prefix: string) => `${prefix}/uni-api/v1/auth/logout`;
const updatePasswordUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/auth/password/update`;
const resetUserPasswordUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/auth/password/reset`;

export {
    authUserAssetLoadUrl,
    authUserLoginUrl,
    loginByCodeUrl,
    authUserTokenUrl,
    authUserLogoutUrl,
    authUserAuthorizeUrl,
    updatePasswordUrl,
    resetUserPasswordUrl
}
