const authPlatformUrl = (prefix: string) => `${prefix}/plat-api/v1/internal/auth/register`;

/**
 * 登录平台的接口地址
 */
const authUserLoginUrl = (prefix: string) => `${prefix}/uni-auth/v1/oauth/login`;
const authUserAuthorizeUrl = (prefix: string) => `${prefix}/uni-auth/v1/oauth/authorize`;
const authUserTokenUrl = (prefix: string) => `${prefix}/uni-auth/v1/oauth/token`;
const authUserLogoutUrl = (prefix: string) => `${prefix}/uni-auth/v1/oauth/sso/logout`;

const getCaptchaCodeUrl = (prefix: string) => `${prefix}/uni-auth/v1/oauth/captcha/code`;

/**
 * 获取登录后用户资源的信息
 */
const authUserAssetLoadUrl = (prefix: string) => `${prefix}/uni-auth/v1/token/auth-user-assets`;

export {
    authPlatformUrl,
    authUserLoginUrl,
    authUserAuthorizeUrl,
    authUserTokenUrl,
    authUserLogoutUrl,
    authUserAssetLoadUrl,
    getCaptchaCodeUrl
}
