/**
 * 保存领域用户信息
 * 参数 {
 *     realmId: '',
 *     userId: '',
 *     name: '',
 *     phone: '',
 *     props: {}
 * }
 */
const realmUserSaveUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/save`;

const updateUserCacheUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/cache/update`;
const resetUserPasswordUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/auth/password/reset`;
const saveRealmUserAccountUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/account/save`;
const lookupRealmUserAccountUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/account/lookup`;

const realmUserLookupUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/query/lookup`;
const realmLoadUserPageUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/query/page`;
const platformLoadUserPageUrl = (prefix: string) => `${prefix}/uni-api/v1/platform/user/query/page`;

/**
 * 按层次分页读取领域用户的分组，根据传入的平台ID过滤掉已经加入平台的分组
 * 此方法会根据parentId查询其子分组信息
 *
 * 参数 {
 *     realmId: '',
 *     parentId: '',
 *     current: 1,
 *     pageSize: 10,
 *     name: ''
 *     filter: {
 *         platformId: '',
 *         groupId: '',
 *     }
 * }
 */
const realmUserGroupLevelPageUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/group/level/page`;
/**
 * 把领域用户分组分配给平台
 * 参数 {
 * realmId: '',
 * tenantId: '',
 * platformId: '',
 * items: []
 * }
 */
const realmAssignPlatsToGroupUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/group/assign-platform/to-group`;
const realmAssignGroupsToPlatformUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/group/assign-group/to-platform`;
const realmSaveGroupsOfPlatformUrl = (prefix: string) => `${prefix}/uni-api/v1/realm//user/group/of-platform/save`;
const realQueryPlatformsInGroupUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/group/query/assign-platform/page`;
const realQueryCanAssignPlatformsUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/group/query/can-assign-platforms/page`;

/**
 * 移除分配给平台的领域用户分组
 * 参数 {
 *     realmId: '',
 *     platformId: '',
 *     groups: [],
 * }
 */
const realmRemoveUserGroupFromPlatformUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/group/assign-platform/remove`;
/**
 * 分页查询分配给平台的领域用户分组
 * 参数 {
 *     realmId: '',
 *     platformId: '',
 *     groups: [],
 * }
 */
const realmUserGroupOfRealmListUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/group/of-realm/list`;
const realmUserGroupOfPlatformListUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/group/of-platform/list`;

/**
 * 把用户加入指定用户分组
 * 参数 {
 *     realmId: '',
 *     groupId: '',
 *     users: []
 * }
 */
const realmAddUserToUserGroupUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/join-to-group`;
/**
 * 把领域用户分组分配给指定用户
 * 参数 {
 *     realmId: '',
 *     userId: '',
 *     groups: [],
 * }
 */
const realmAddUserGroupToUserUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/group/assign-to-user`;
/**
 * 把用户从分组中移除
 * 参数 {
 *     realmId: '',
 *     groupId: '',
 *     users: [],
 * }
 */
const realmRemoveUserFromUserGroupUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/user/remove-from-group`;
/**
 * 把分组列表从指定用户中移除
 * 参数 {
 *     realmId: '',
 *     userId: '',
 *     groups: [],
 * }
 */
const realRemoveUserGroupFromUserUrl = (prefix: string) => `${prefix}/uni-api/v1/realm/group/remove-from-user`;
/**
 * 分页查询平台信息
 * 参数 {
 * realmId: '',
 * tenantId: '',
 * name: '',
 * state: 0,
 * }
 * @returns
 */

const platformUpdateCacheUrl = (prefix: string) => `${prefix}/uni-api/v1/platform/cache/update`;
const platformLookupUrl = (prefix: string) => `${prefix}/plat-api/v1/internal/platform/query/lookup`;
const getUserAllAssetsUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/query/user-assets`;

/**
 * 保存资源信息
 * 参数 {
 *     server: '',
 *     targetId: '',
 *     asset: {}
 * }
 */
const saveAssetUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/save`;
const moveAssetUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/move`;
/**
 * 删除资源
 * 参数 {
 *     server: '',
 *     targetId: '',
 *     asset: {
 *         id: '',
 *         type: '',
 *     }
 * }
 */
const deleteAssetUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/delete`;
/**
 * 分页查询指定资源，此方法不会对资源分层，只查询满足条件的所有资源
 * 参数 {
 * server: '',
 * targetId: '',
 * tenantId: '',
 * assetId: '1',
 * assetType: 'PERMIT',
 * parentId: '',
 * name: '',
 * code: '',
 * state: 0,
 * }
 * 返回值 {
 * code: 0,
 * message: '',
 * data: [{
 * id: '',
 * code: '',
 * name: '',
 * subType: '',
 * parentId: '',
 * }]
 * }
 */
const loadAssetPageUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/query/page`;
const loadMoveAssetPageUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/query/move/page`;
/**
 * 获取指定资源所有信息，包括其层次结构信息
 * 参数  {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * assetId: '1',
 * assetType: 'PERMIT',
 * name: '',
 * }
 */
const loadAssetLevelUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/query/level/all`;
/**
 * 按层级分页查询资源信息，必须传入父资源ID，如果父资源ID为空，则查询顶层所有资源
 * 参数 {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * assetId: '1',
 * assetType: 'PERMIT',
 * parentId: '',
 * name: '',
 * code: '',
 * state: 0,
 * filter: {
 *     holds: [],
 *     targets: [],
 * }
 * }
 * 返回值 {
 * code: 0,
 * message: '',
 * data: [{
 * id: '',
 * code: '',
 * name: '',
 * subType: '',
 * parentId: '',
 * }]
 * }
 */
const loadAssetLevelPageUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/query/level/page`;

/**
 * 分页查询指定资源拥有的所有资源信息，比如查询指定分组下所有的权限信息
 * 参数 {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * current: 1,
 * pageSize: 10,
 * holdId: '1',
 * holdType: 'GROUP',
 * assetType: 'PERMIT'
 * }
 */
const loadAssetOfHolderPageUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/relation/of-holder/page`;
/**
 * 分页查询拥有指定资源的所有资源信息，比如查询拥有指定权限的所有分组信息
 * 参数 {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * current: 1,
 * pageSize: 10,
 * assetId: '1',
 * assetType: 'PERMIT',
 * holdType: 'GROUP'
 * }
 */
const loadHolderForAssetPageUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/relation/to-holders/page`;
/**
 * 保存资源持有信息
 * 参数 {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * holdId: '',
 * holdType: '',
 * assets: [{
 *  assetId: '',
 *  assetType: '',
 * }
 * ]
 * }
 * 返回结果
 */
const saveAssetOfHolderUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/relation/of-holder/save`;
/**
 * 保存资源持有信息
 * 参数 {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * assetId: '',
 * assetType: '',
 * holders: [{
 *  holdId: '',
 *  holdType: '',
 * }
 * ]
 * }
 * @returns
 */
const saveHolderForAssetUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/relation/to-holder/save`;
/**
 * 移除持有的资源
 * 参数 {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * holeId: '',
 * holdType: '',
 * assets: [{
 *  assetId: '',
 *  assetType: '',
 * }
 * ]
 * }
 * @returns
 */
const removeAssetOfHolderUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/relation/of-holder/remove`;
/**
 * 保存资源属性
 * 参数 {
 *     server: '',   // targetId所属服务类型
 *     targetId: '', // 资源所在目标ID
 *     assetId: '',
 *     assetType: '',
 *     props: {
 *         name: value
 *     }
 * }
 */
const saveAttributeOfAssetUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/attribute/save`;
/**
 * 删除资源属性
 * 参数 {
 *     server: '',   // targetId所属服务类型
 *     targetId: '', // 资源所在目标ID
 *     assetId: '',
 *     assetType: '',
 *     props: [
 *         '11','22'
 *     ]
 * }
 */
const removeAttributeOfAssetUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/attribute/remove`;
/**
 * 读取资源属性
 * 参数 {
 *     server: '',   // targetId所属服务类型
 *     targetId: '', // 资源所在目标ID
 *     assetId: '',
 *     assetType: '',
 * }
 * @return {
 *     assetId: '',
 *     assetType: '',
 *     props: {}
 * }
 */
const loadAttributeOfAssetUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/query/attribute/load`;

const setMenuToHomeUrl = (prefix: string) => `${prefix}/uni-api/v1/asset/menu/set-home`;

export {
    moveAssetUrl,
    saveAssetUrl,
    deleteAssetUrl,
    loadAssetPageUrl,
    loadMoveAssetPageUrl,
    loadAssetLevelUrl,
    loadAssetLevelPageUrl,

    saveAssetOfHolderUrl,
    saveHolderForAssetUrl,
    removeAssetOfHolderUrl,
    loadAssetOfHolderPageUrl,
    loadHolderForAssetPageUrl,
    realmUserSaveUrl,
    updateUserCacheUrl,
    resetUserPasswordUrl,
    saveRealmUserAccountUrl,
    lookupRealmUserAccountUrl,

    realmUserLookupUrl,
    realmUserGroupOfRealmListUrl,
    realmUserGroupOfPlatformListUrl,
    realmLoadUserPageUrl,
    platformLoadUserPageUrl,
    realmUserGroupLevelPageUrl,
    realmAssignPlatsToGroupUrl,
    realQueryPlatformsInGroupUrl,
    realmSaveGroupsOfPlatformUrl,
    realmAssignGroupsToPlatformUrl,
    realQueryCanAssignPlatformsUrl,
    realmRemoveUserGroupFromPlatformUrl,
    realmAddUserToUserGroupUrl,
    realmAddUserGroupToUserUrl,
    realmRemoveUserFromUserGroupUrl,
    realRemoveUserGroupFromUserUrl,

    platformUpdateCacheUrl,
    platformLookupUrl,
    getUserAllAssetsUrl,

    saveAttributeOfAssetUrl,
    removeAttributeOfAssetUrl,
    loadAttributeOfAssetUrl,

    setMenuToHomeUrl,
}
