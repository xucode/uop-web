import {
    authUserAssetLoadUrl,
    authUserAuthorizeUrl,
    authUserLoginUrl,
    authUserLogoutUrl,
    authUserTokenUrl,
    authPlatformUrl,
    getCaptchaCodeUrl
} from "@/apis/sop/auth-url";
import {ssoRequest} from "@/apis/request";

export const authPlatform = async (params: any) => ssoRequest(
    {
        url: authPlatformUrl(''),
        method: 'post',
        data: params
    });

export const userLogin = async (params: any) => ssoRequest(
    {
        url: authUserLoginUrl(''),
        method: 'post',
        data: params
    });

/**
 * 动态路由接口
 */
export const loadAuthUserAsset = async () => ssoRequest(
    {
        url: authUserAssetLoadUrl(''),
        method: 'post'
    })

export function loginByCode(code: string) {
    return ssoRequest({
        url: loginByCodeUrl('', code),
        method: 'post'
    })
}

export const userToken = async (code: any) => ssoRequest(
    {url: authUserTokenUrl('') + `?code=${code}`, method: 'post', data: {}});

/**
 * 下面是单点登录接口
 * @param params
 * @param clientId
 * @param clientSecret
 * @param platToken
 */
export const userAuthorize = async (params: any, clientId: string,
                                    clientSecret: string, platToken: string) => ssoRequest({
    url: authUserAuthorizeUrl(''),
    method: 'post',
    data: params
}, clientId, clientSecret, platToken);

export const userLogout = async () => ssoRequest(
    {url: authUserLogoutUrl(''), method: 'post', data: {}});


export const getCaptchaCode = async () => ssoRequest(
    {url: getCaptchaCodeUrl(''), method: 'post'});
