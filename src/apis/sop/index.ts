import {useStore} from "@/stores";

import {
    deleteAssetUrl,
    getUserAllAssetsUrl,
    loadAssetLevelPageUrl,
    loadAssetLevelUrl,
    loadAssetOfHolderPageUrl,
    loadAttributeOfAssetUrl,
    loadHolderForAssetPageUrl,
    loadMoveAssetPageUrl,
    moveAssetUrl,
    platformLoadUserPageUrl,
    platformLookupUrl,
    platformUpdateCacheUrl,
    removeAssetOfHolderUrl,
    removeAttributeOfAssetUrl,
    resetUserPasswordUrl,
    saveAssetOfHolderUrl,
    saveAssetUrl,
    saveAttributeOfAssetUrl,
    saveHolderForAssetUrl,
    setMenuToHomeUrl,
    updateUserCacheUrl,
} from "@/apis/sop/url";
import {ssoRequest} from "@/apis/request";

export const requestRealm = async (params: any) => {
    return await ssoRequest(params);
}

export const requestPlatform = async (params: any) => {
    const store = useStore();
    const currentPl: any = store.getCurrentPlatform();
    params.data['platformId'] = currentPl?.id;
    return await ssoRequest(params);
}

// --------------------- 下面是资源操作接口

export const getUserAllAssets = async (params: any) => ssoRequest(
    {url: getUserAllAssetsUrl(''), method: 'post', data: params});

/**
 * 保存资源持有信息
 * @param params {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * holdId: '',
 * holdType: '',
 * assets: [{
 *  assetId: '',
 *  assetType: '',
 * }
 * ]
 * }
 * @returns
 */
export const saveAssetOfHolder = async (params: any) => ssoRequest(
    {url: saveAssetOfHolderUrl(''), method: 'post', data: params});

/**
 * 保存资源持有信息
 * @param params {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * assetId: '',
 * assetType: '',
 * holders: [{
 *  holdId: '',
 *  holdType: '',
 * }
 * ]
 * }
 * @returns
 */
export const saveHolderForAsset = async (params: any) => ssoRequest(
    {url: saveHolderForAssetUrl(''), method: 'post', data: params});

/**
 * 按层级分页查询资源信息，必须传入父资源ID，如果父资源ID为空，则查询顶层所有资源
 * @param params {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * assetId: '1',
 * assetType: 'PERMIT',
 * parentId: '',
 * name: '',
 * code: '',
 * state: 0,
 * filter: {
 *     holds: [
 *         {
 *             id: '',
 *             type: '',
 *         }
 *     ],
 *     targets: [
 *         {
 *             id: '',
 *             type: '',
 *         }
 *     ],
 * }
 * }
 * @returns {
 * code: 0,
 * message: '',
 * data: [{
 * id: '',
 * code: '',
 * name: '',
 * subType: '',
 * parentId: '',
 * }]
 * }
 */
export const loadAssetLevelPage = async (params: any) => ssoRequest(
    {url: loadAssetLevelPageUrl(''), method: 'post', data: params});

/**
 * 获取指定资源所有信息，包括其层次结构信息
 * @param params  {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * assetId: '1',
 * assetType: 'PERMIT',
 * name: '',
 * }
 */
export const loadAssetLevel = async (params: any) => ssoRequest(
    {url: loadAssetLevelUrl(''), method: 'post', data: params});

export const loadMoveAssetPage = async (params: any) => ssoRequest(
    {url: loadMoveAssetPageUrl(''), method: 'post', data: params});

export const moveAsset = async (asset: any) => ssoRequest(
    {url: moveAssetUrl(''), method: 'post', data: asset});

/**
 * 保存资源信息
 * @param asset 资源信息 {
 *     server: '',
 *     targetId: '',
 *     asset: {}
 * }
 */
export const saveAsset = async (asset: any) => ssoRequest(
    {url: saveAssetUrl(''), method: 'post', data: asset});

/**
 * 删除资源
 * @param asset {
 *     server: '',
 *     targetId: '',
 *     asset: {
 *         id: '',
 *         type: '',
 *     }
 * }
 */
export const deleteAsset = async (asset: any) => requestPlatform(
    {url: deleteAssetUrl(''), method: 'post', data: asset});

/**
 * 分页查询指定资源拥有的所有资源信息，比如查询指定分组下所有的权限信息
 * @param params {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * current: 1,
 * pageSize: 10,
 * holdId: '1',
 * holdType: 'GROUP',
 * assetType: 'PERMIT'
 * }
 * @returns
 */
export const loadAssetsForHolderByPage = async (params: any) => ssoRequest(
    {url: loadAssetOfHolderPageUrl(''), method: 'post', data: params});

/**
 * 分页查询拥有指定资源的所有资源信息，比如查询拥有指定权限的所有分组信息
 * @param params {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * current: 1,
 * pageSize: 10,
 * assetId: '1',
 * assetType: 'PERMIT',
 * holdType: 'GROUP'
 * }
 * @returns
 */
export const loadHolderForAssetPage = async (params: any) => ssoRequest(
    {url: loadHolderForAssetPageUrl(''), method: 'post', data: params});

/**
 * 移除持有的资源
 * @param params {
 * server: '',   // targetId所属服务类型
 * targetId: '', // 资源所在目标ID
 * tenantId: '',
 * holeId: '',
 * holdType: '',
 * assets: [{
 *  assetId: '',
 *  assetType: '',
 * }
 * ]
 * }
 * @returns
 */
export const removeAssetOfHolder = async (params: any) => ssoRequest(
    {url: removeAssetOfHolderUrl(''), method: 'post', data: params});

/**
 * 读取资源属性
 * @param params {
 *     server: '',   // targetId所属服务类型
 *     targetId: '', // 资源所在目标ID
 *     assetId: '',
 *     assetType: '',
 * }
 * @return {
 *     assetId: '',
 *     assetType: '',
 *     props: {}
 * }
 */
export const loadAttributeOfAsset = async (params: any) => ssoRequest(
    {url: loadAttributeOfAssetUrl(''), method: 'post', data: params});

/**
 * 保存资源属性
 * @param params {
 *     server: '',   // targetId所属服务类型
 *     targetId: '', // 资源所在目标ID
 *     assetId: '',
 *     assetType: '',
 *     props: {
 *         name: value
 *     }
 * }
 */
export const saveAttributeOfAsset = async (params: any) => ssoRequest(
    {url: saveAttributeOfAssetUrl(''), method: 'post', data: params});

/**
 * 删除资源属性
 * @param params {
 *     server: '',   // targetId所属服务类型
 *     targetId: '', // 资源所在目标ID
 *     assetId: '',
 *     assetType: '',
 *     props: [
 *         '11','22'
 *     ]
 * }
 */
export const removeAttributeOfAsset = async (params: any) => ssoRequest(
    {url: removeAttributeOfAssetUrl(''), method: 'post', data: params});

export const setMenuToHome = async (params: any) => ssoRequest(
    {url: setMenuToHomeUrl(''), method: 'post', data: params});

// --------------------- 下面领域和领域用户相关操作接口
export const updateUserCache = async (user: any) => requestRealm(
    {url: updateUserCacheUrl(''), method: 'post', data: user})
export const resetUserPassword = async (user: any) => requestRealm(
    {url: resetUserPasswordUrl(''), method: 'post', data: user})

export const platformLoadUserPage = async (params: any) => ssoRequest(
    {url: platformLoadUserPageUrl(''), method: 'post', data: params});

// --------------------- 下面是平台相关操作接口
export const lookupPlatform = async (pl: any) => ssoRequest(
    {url: platformLookupUrl(''), method: 'post', data: pl})

export const loadLoginLogPage = async (params: any) => requestPlatform(
    {url: '/uni-auth/v1/token/log/query/login/page', method: 'post', data: params})

export const queryAssetLabels = async (params: any) => ssoRequest(
    {url: '/uni-api/v1/asset/labels/query/load', method: 'post', data: params})

// ---------------------------------- 字典相关API
export const saveDictClass = async (params: any) => ssoRequest(
    {url: '/uni-api/v1/platform/dict-class/save', method: 'post', data: params})
export const removeDictClass = async (params: any) => ssoRequest(
    {url: '/uni-api/v1/platform/dict-class/remove', method: 'post', data: params})
export const loadSystemDictPage = async (params: any) => ssoRequest(
    {url: '/uni-api/v1/platform/dict-class/query/page', method: 'post', data: params})

export const saveDictValue = async (params: any) => ssoRequest(
    {url: '/uni-api/v1/platform/dict-value/save', method: 'post', data: params})
export const removeDictValue = async (params: any) => ssoRequest(
    {url: '/uni-api/v1/platform/dict-value/remove', method: 'post', data: params})
export const loadSystemDictValuePage = async (params: any) => ssoRequest(
    {url: '/uni-api/v1/platform/dict-value/query/page', method: 'post', data: params})
