import {
    authUserAssetLoadUrl,
    authUserAuthorizeUrl,
    authUserLoginUrl,
    authUserLogoutUrl,
    authUserTokenUrl,
    loginByCodeUrl,
    updatePasswordUrl
} from "@/apis/url-conf";
import {bsRequest, request} from "@/apis/request";
import {useStore} from "@/stores";

export const buildHeaders = () => {
    const store = useStore();
    const cred = store.getCredential();
    return {
        access_token: cred.accessToken,
        client_id: import.meta.env.VITE_APP_CLIENT_ID,
        client_secret: import.meta.env.VITE_APP_CLIENT_SECRET,
        terminal_code: 'WEB',
        platform: import.meta.env.VITE_APP_PLATFORM
    }
}

export const userLogin = async (params: any, codeInfo: any) => request(
    {
        url: authUserLoginUrl(''),
        method: 'post',
        data: params,
        headers: {
            captcha_key: codeInfo.key,
            captcha_code: codeInfo.code,
        }
    });

/**
 * 下面是单点登录接口
 * @param params
 * @param codeInfo
 */
export const userAuthorize = async (params: any, codeInfo: any) => request({
    url: authUserAuthorizeUrl(''),
    method: 'post',
    data: params,
    headers: {
        captcha_key: codeInfo.key,
        captcha_code: codeInfo.code,
    }});

/**
 * 动态路由接口
 */
export const loadAuthUserAsset = async () => request(
    {
        url: authUserAssetLoadUrl(''),
        method: 'post'
    })

export function loginByCode(code: string) {
    return request({
        url: loginByCodeUrl('', code),
        method: 'post'
    })
}

export const userToken = async (code: any) => request(
    {url: authUserTokenUrl('') + `?code=${code}`, method: 'post', data: {}});

export const userLogout = async () => request(
    {url: authUserLogoutUrl(''), method: 'post', data: {}});

export const updatePassword = async (user: any) => request(
    {url: updatePasswordUrl(''), method: 'post', data: user})

export const hello = async () => bsRequest(
    {url: '/test/hello', method: 'post', data: {}});
