import {useStore} from "@/stores";

export function useTerminal(name: string) {
    const store = useStore();

    const productCode = () => {
        return store.products[name]?.productCode || 'default';
    }

    const onTerminal = (val: string) => {
        if (!store.products[name]) {
            store.products[name] = {};
        }
        if (store.products[name].productCode !== val) {
            store.products[name].productCode = val;
        }
    }

    return {
        productCode,
        onTerminal,
    }
}
