import {useStore} from "@/stores";
import router from "@/router";

export const hasMenuPermit = (permit: string) => {
    const menuPermits: any = router.currentRoute.value.meta.permits;
    return menuPermits && menuPermits.indexOf(permit) != -1;
}

export const hasPermit = (permit: string) => {
    const permitCodes: any = useStore().getUserPermits();
    return permitCodes && permitCodes.indexOf(permit) != -1;
}
