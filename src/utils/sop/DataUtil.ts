export function buildAsset(item: Record<string, any>, type: string, props: any) {
    return {
        id: item.id,
        name: item.name,
        code: item.code || '',
        type: type,
        state: item.state,
        rootId: item.rootId || item.id,
        parentId: item.parentId || '',
        classify: item.classify,
        level: item.level || 0,
        props: props
    };
}
