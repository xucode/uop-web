import {toRaw} from "vue";
import {useStore} from "@/stores";

export function useCurrent(name: string) {
    const store = useStore();

    const current = () => {
        const item = store.getCurrent(name);
        return item ? toRaw(item.item) : {};
    }

    const updateCurrent = async (item: any, tag: string='route') => {
        if (item) {
            if (!item.id) {
                console.log('---- 更新当前处理项, id为空', item, name);
            }
            store.updateCurrent(name, item, tag);
        }
    }

    return {
        current,
        updateCurrent,
    }
}
