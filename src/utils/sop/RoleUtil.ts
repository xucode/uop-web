import {getUserAllAssets} from "@/apis/sop";
import {currentNames, TYPES} from "@/stores/conf";
import {useStore} from "@/stores";

const loadUserAllAssets = async (store: any, userId: string) => {
    const platform: any = store.getCurrentPlatform();
    return await getUserAllAssets({
        holdId: userId,
        holdType: TYPES.USER,
        platformId: platform.id,
    });
}

export const refreshUserAssets = async (name: string) => {
    let detail: any = {};

    console.log('--- refreshUserAssets', name);

    if (name == currentNames.PLAT_USER ||
        name == currentNames.ROLE) {
        const store: any = useStore();
        const curRow: any = store.getCurrent(name);

        if (curRow && curRow.item) {
            const result: any = await loadUserAllAssets(store, curRow.item.id);
            if (result && result.data) {
                detail = result.data;
            }
            store.updateCurrentDetail(name, detail);
            return true;
        }
    }
    return false;
}
