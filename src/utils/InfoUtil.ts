import {ElMessage, ElMessageBox} from "element-plus";

export function showInfo(content: string, title: string='温馨提示') {
    ElMessageBox.alert(content, title, {
        confirmButtonText: '确定',
        type: 'success'
    }).then(r => {})
}

export function showError(content: string, title: string='温馨提示') {
    ElMessageBox.alert(content, title, {
        confirmButtonText: '确定',
        type: 'error'
    }).then(r => {})
}

export function showWarn(content: string, title: string='温馨提示') {
    ElMessageBox.alert(content, title, {
        confirmButtonText: '确定',
        type: 'warning'
    }).then(r => {})
}

export function msgInfo(content: string) {
    ElMessage({
        message: content,
        type: 'success',
    })
}

export function msgWarn(content: string) {
    ElMessage({
        message: content,
        type: 'warning',
    })
}

export function msgError(content: string,
                         duration: number=3000,
                         showClose: boolean=true) {
    ElMessage({
        message: content,
        type: 'error',
        offset: 32,
        duration: duration,
        showClose: showClose,
    });
}

export function msgConfirm(content: string, cb: any) {
    ElMessageBox.confirm(
        content,
        '温馨提示',
        {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'info',
        }
    ).then(async () => {
        if (cb) {
            cb();
        }
    })
}
