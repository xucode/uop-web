function createId(size: number) {
    let result = "";
    const data: any = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];
    for (let i = 0; i < size; i++) {
        const r: any = Math.floor(Math.random() * 16);
        result += data[r];
    }
    return result;
}

export function create20() {
    return createId(20);
}

export function create32() {
    return createId(32);
}


export const checkLongValue = (val: any) => {
    if (!val) {
        return val;
    }

    let result = '';

    for (let i = 0; i < val.length; ++i) {
        if (val[i] == '-') {
            if (result.length == 0) {
                result = '-';
            }
        } else if (/^\d$/.test(val[i])) {
            result += val[i];
        }
    }

    return result;
}

export const checkDoubleValue = (val: any) => {
    if (!val) {
        return val;
    }
    let hasDot = false;
    let result = '';

    for (let i = 0; i < val.length; ++i) {
        if (val[i] == '-') {
            if (result.length == 0) {
                result = '-';
            }
        } else if (/^\d$/.test(val[i])) {
            result += val[i];
        } else  if (!hasDot && val[i] == '.') {
            if (result.length > 1 ||
                (result.length == 1 && result != '-')) {
                result += val[i];
                hasDot = true;
            }
        }
    }

    return result;
}

export const isRegexValid = (regexPattern: any) => {
    try {
        const reg = new RegExp(regexPattern);
        console.log('isRegexValid', reg)
        return !!reg;
    } catch (error) {
        return false;
    }
}

export const filterNameInput = (str: any) => {
    return str.replace(/[^A-Za-z0-9-_:.*=+%]/g, '');
}
