export function sleep(time: any) {
    return new Promise(resolve => setTimeout(resolve, time))
}

const runMethod = async (params: any, runner: any, t: number=300) => {
    let result;
    let startTime: number = new Date().getTime() + t;

    if (runner) {
        result = await runner(params);
    }
    startTime -= new Date().getTime();

    if (startTime > 0) {
        await sleep(startTime);
    }

    return result;
}

export {
    runMethod,
};
