export function findTreeParent(data: any, row: any) : MenuType | undefined {
  let parent: MenuType | undefined = undefined;

  data.forEach((item: any) => {
    if (row.parentId === item.id) {
      return item;
    } else if (item.children && item.children.length > 0) {
      parent = findTreeParent(item.children, row);
      if (parent) {
        return parent;
      }
    }
  });

  return parent;
}

export function computeTreeData(data: any, row: any, filter: any=undefined){
  const treeData: any = {
    parent: undefined,
    data: [],
  };

  data.forEach((item: any) => {
    if (row.parentId === item.id) {
      treeData.parent = item;
    }
    if (item.id !== row.id && (!filter || !filter(item))) {
      const subItem = {
        value: item.id,
        label: item.name,
        children: undefined,
      };
      treeData.data.push(subItem);

      if (item.children && item.children.length > 0) {
        subItem.children = computeTreeData(item.children, row, filter).data;
      }
    }
  });

  return treeData;
}
